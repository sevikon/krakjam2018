import Phaser from 'phaser'
// import {BonusTypes} from './Bonus';
import {centerGameObjects, produceJumpingNumber, produceJumpingText} from '../utils';
import config from '../config';
import soundManager from '../instances/SoundManager'

const STATE_UP = 'STATE_UP';
const STATE_DOWN = 'STATE_DOWN';
const STATE_LEFT = 'STATE_LEFT';
const STATE_RIGHT = 'STATE_RIGHT';

const MIN_TURBO = 1;
const MAX_TURBO = 3;
const TURBO_SPEED = 0.04;

export default class Player extends Phaser.Sprite {

    constructor({gameState, x, y, asset}, {player}) {
        super(game, x, y, asset);
        centerGameObjects([this]);
        this.playerNumber = playerNumber;
        this.direction = 'left';
        this.velocity = config.initialPlayerVelocity;
        this.angleVelocity = config.angleVelocity;
        this.score = 0;

        game.physics.enable(this, config.physicsType);

        this.body.collideWorldBounds = true;
        this.keys = {};
        this.won = false;
        this.turbo = MIN_TURBO;

        const playerKeys = config[playerNumber].keys;

        for (const action in playerKeys) {
            if (playerKeys.hasOwnProperty(action)) {
                this.keys[action] = game.input.keyboard.addKey(playerKeys[action]);
            }
        }

        this.gameState = gameState;

        // this.keys.destroySack.onDown.add(() => {
        //     this.pickSack();
        // }, this);

        // const path = `${playerNumber}/walk/`;

        // this.animations.add('walkRight', Phaser.Animation.generateFrameNames(path, 4, 4, '.png', 4), 10, true, false);
        // this.animations.add('walkLeft', Phaser.Animation.generateFrameNames(path, 2, 2, '.png', 4), 10, true, false);
        // this.animations.add('walkUp', Phaser.Animation.generateFrameNames(path, 3, 3, '.png', 4), 10, true, false);
        // this.animations.add('walkDown', Phaser.Animation.generateFrameNames(path, 1, 1, '.png', 4), 10, true, false);
        // this.animations.play('walkDown');

        this.body.setSize(0.4 * this.body.width, this.body.height / 4, 0.3 * this.body.width, 0.75 * this.body.height);

    }

    stopMovement() {
        // this.body.velocity.y = 0;
    }

    calculateVelocity() {
        this.body.velocity.x = this.velocity * Math.cos(this.angle / 180 * Math.PI) * this.turbo;
        this.body.velocity.y = this.velocity * Math.sin(this.angle / 180 * Math.PI) * this.turbo;
    }

    moveLeft() {
        // this.body.velocity.x = -this.velocity;
        this.angle -= this.angleVelocity;
        this.direction = STATE_LEFT;
        // this.body.velocity.y -= 1;
        // this.body.velocity.x -= 1;
        // this.animations.play('walkLeft');
    }

    moveRight() {
        // this.body.velocity.x = this.velocity;
        this.direction = STATE_RIGHT;
        this.angle += this.angleVelocity;
        // this.body.velocity.x += 1;
        // this.body.velocity.y += 1;
        // this.animations.play('walkRight');
    }

    moveUp() {
        this.turbo += TURBO_SPEED;
        if (this.turbo > MAX_TURBO) {
            this.turbo = MAX_TURBO;
        }
        // this.body.velocity.y = -this.velocity;
        // this.direction = STATE_UP;
        // this.animations.play('walkUp');
    }

    moveDown() {
        this.turbo -= TURBO_SPEED;
        if (this.turbo < MIN_TURBO) {
            this.turbo = MIN_TURBO;
        }
        // this.body.velocity.y = this.velocity;
        // this.direction = STATE_DOWN;
        // this.animations.play('walkDown');
    }

    //Wywoływana 60 razy na sekundę
    update() {

        // this.stopMovement();

        if (this.keys.left.isDown) {
            this.moveLeft();
        }

        if (this.keys.right.isDown) {
            this.moveRight();
        }

        if (this.keys.down.isDown) {
            this.moveDown();
        }

        if (this.keys.up.isDown) {
            this.moveUp();
        }

        this.calculateVelocity();

        // if (this.keys.placeSack.isDown) {
        //     this.placeSack();
        // }

    }
}
