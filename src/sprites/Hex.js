import Phaser from 'phaser';
import Robot from '../sprites/Robot';
import Transmitter from './Transmitter';
import config from '../config';
import {centerGameObjects} from '../utils';
import Bonus from './Bonus';
import soundManager from '../instances/SoundManager';

const {PLAYER_1, BONUS_KIND_SHOOT, ROBOT_MARGIN, BONUS_KIND_OBSTACLE, BONUS_KIND_SPEED, BONUS_KIND_FREEZE} = config;

export default class Hex extends Phaser.Sprite {

    constructor({game, x, y, asset}, {idx, idy, clickCallback, shootCallback, obstacleCallback, freezeCallback}) {
        super(game, x, y, asset);
        this.idx = idx;
        this.idy = idy;
        this.anchor.set(0, 0);
        this.inputEnabled = true;
        this.input.pixelPerfectClick = true;
        this.clickCallback = clickCallback;
        this.shootCallback = shootCallback;
        this.obstacleCallback = obstacleCallback;
        this.freezeCallback = freezeCallback;
        this.transmitter = null;
        this.events.onInputDown.add(() => {
            clickCallback({idx, idy})
        });
        this.events.onInputOver.add(() => {
            if (this.state === Hex.states.OBSTACABLE) {
                this.loadTexture('hex_7');
            }
        });
        this.events.onInputOut.add(() => {
            if (this.state === Hex.states.OBSTACABLE) {
                this.loadTexture('hex_6');
            }
        });

        this.state = Hex.states.CLEAR;
        this.robot = null;
    }

    addRobot({playerNumber}) {
        const {idx, idy} = this;
        const robot = new Robot({
            game: this.game,
            x: this.x,
            y: this.y - ROBOT_MARGIN - (playerNumber === PLAYER_1 ? 10 : 20),
            asset: `robot_${playerNumber === PLAYER_1 ? '1' : '2'}`
        }, {
            playerNumber: playerNumber,
            shootCallback: () => {
                this.shootCallback({idx, idy})
            },
            clickCallback: () => {
                this.clickCallback({idx, idy})
            },
            freezeCallback: () => {
                this.freezeCallback({idx, idy})
            },
            obstacleCallback: () => {
                this.obstacleCallback({idx, idy})
            }
        });
        centerGameObjects([robot]);
        this.robot = this.game.add.existing(robot);
        return this.robot;
    }

    addRobotBasedOn(baseRobot) {
        const addedRobot = this.addRobot({playerNumber: baseRobot.playerNumber});
        baseRobot.shootIcon && addedRobot.enableShoot();
        baseRobot.freezeIcon && addedRobot.enableFreeze();
        baseRobot.obstacleIcon && addedRobot.enableObstacle();
    }

    destroyRobot(data) {
        this.robot.destroyRobot(data);
        this.robot = null;
    }

    //Funkcja odpowiadająca za wyświetlenie wieży transmisyjnych
    addTransmitter({playerNumber}) {
        const {idx, idy} = this;

        const p = playerNumber === PLAYER_1 ? 91 : 91;
        const r = playerNumber === PLAYER_1 ? -20 : 20;
        this.transmitter = new Transmitter({
            game: this.game,
            x: this.x - r,
            y: this.y - p,
            asset: `transmitter_${playerNumber === PLAYER_1 ? '1' : '2'}`
        }, {
            clickCallback: () => {
                this.clickCallback({idx, idy})
            },
            playerNumber: playerNumber
        });
        centerGameObjects([this.transmitter]);
        this.game.add.existing(this.transmitter);
        return this.transmitter;
    }

    addBonus({kind}) {
        this.bonus = new Bonus({
            game: this.game,
            x: this.x,
            y: this.y,
            asset: `bonus_${kind}`
        }, {kind});
        centerGameObjects([this.bonus]);
        this.bonus = this.game.add.existing(this.bonus);
    }

    //Zebranie bonusa
    collectBonus() {
        if (this.robot && !this.robot.hasBonus()) {
            if (this.bonus.kind === BONUS_KIND_SHOOT) {
                this.robot.enableShoot();
            } else if (this.bonus.kind === BONUS_KIND_FREEZE) {
                this.robot.enableFreeze();
            }
            else if (this.bonus.kind === BONUS_KIND_OBSTACLE) {
                this.robot.enableObstacle();
            }
            this.bonus.destroy();
            this.bonus = null;
            soundManager.play('select', false);
        }
    }

    markEnterable(robot) {
        this.state = Hex.states.ENTERABLE;
        this.loadTexture(`hex_${robot === PLAYER_1 ? '2' : '3'}`);
    }

    markShootable() {
        if (this.state !== Hex.states.OBSTRUCTED) {
            this.state = Hex.states.SHOOTABLE;
            this.loadTexture('hex_4');
        }
    }

    markObstacable() {
        this.state = Hex.states.OBSTACABLE;
        this.loadTexture('hex_6');
    }

    markFreezable() {
        if (this.state !== Hex.states.OBSTRUCTED) {
            this.state = Hex.states.FREEZEABLE;
            this.loadTexture('hex_5');
        }
    }

    //Rozwalenie kafelka planszy
    markObstructed() {
        soundManager.play('bum', false);
        this.state = Hex.states.OBSTRUCTED;
        this.loadTexture('hex_8');
    }

    clear() {
        if (!this.transmitter && this.state !== Hex.states.OBSTRUCTED) {
            this.state = Hex.states.CLEAR;
            this.loadTexture('hex');
        }
    }

}

Hex.states = {
    CLEAR: 'CLEAR',
    ENTERABLE: 'ENTERABLE',
    SHOOTABLE: 'SHOOTABLE',
    FREEZEABLE: 'FREEZEABLE',
    OBSTACABLE: 'OBSTACABLE',
    OBSTRUCTED: 'OBSTRUCTED'
};
