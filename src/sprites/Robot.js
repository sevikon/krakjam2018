import Phaser from 'phaser'
import {centerGameObjects} from '../utils';
import ShootIcon from './ShootIcon';
import config from '../config';
import FreezeIcon from './FreezeIcon';
import ObstacleIcon from './ObstacleIcon';
import soundManager from '../instances/SoundManager';

const {PLAYER_1} = config;

export default class extends Phaser.Sprite {

    constructor({game, x, y, asset}, {playerNumber, clickCallback, shootCallback, obstacleCallback, freezeCallback}) {
        super(game, x, y, asset);
        this.game = game;
        this.anchor.set(0, 0);
        this.inputEnabled = true;
        this.playerNumber = playerNumber;
        this.events.onInputDown.add(clickCallback, this);
        this.shootCallback = shootCallback;
        this.obstacleCallback = obstacleCallback;
        this.freezeCallback = freezeCallback;
        this.mainAsset = asset;
        this.freezeState = 0;
        this.bonus = false;
        this.walkAsset = `robot_${playerNumber === PLAYER_1 ? '1' : '2'}_walk`;
        this.destroyAsset = `robot_${playerNumber === PLAYER_1 ? '1' : '2'}_explosion`;
        this.frozenAsset = `robot_${playerNumber === PLAYER_1 ? '1' : '2'}_frozen`;

        this.loadTexture(this.walkAsset);
        this.animations.add(this.walkAsset);

        this.loadTexture(this.destroyAsset);
        this.animations.add(this.destroyAsset);

        this.loadTexture(this.frozenAsset);
        this.freezeAnim2 = this.animations.add(this.frozenAsset, [0, 1, 2]);
        this.freezeAnim = this.animations.add(this.frozenAsset, [3, 4, 5]);

        this.loadTexture(this.mainAsset);
    }

    addBonus() {
        this.bonus = true;
    }

    removeBonus() {
        this.bonus = false;
    }

    hasBonus() {
        return this.bonus;
    }

    drawLaser({x, y}, {callback}) {
        soundManager.play('laser', false);
        const disX = x - this.x;
        const disY = y - this.y;
        const distance = Math.sqrt(disX * disX + disY * disY);
        const angle = Math.atan2(disY, disX) * 180 / Math.PI;

        const h = 12;
        const w = distance;
        const posX = this.x;
        const posY = this.y;
        this.moving = false;

        const myBmp = this.game.add.bitmapData(1, h);
        const myGrd = myBmp.context.createLinearGradient(0, 0, 0, h);
        if (this.playerNumber === PLAYER_1) {
            myGrd.addColorStop(0, config.LINE_BLUE_LIGHT);
            myGrd.addColorStop(0.5, config.LINE_BLUE_DARK);
            myGrd.addColorStop(1, config.LINE_BLUE_LIGHT);
        } else {
            myGrd.addColorStop(0, config.LINE_RED_LIGHT);
            myGrd.addColorStop(0.5, config.LINE_RED_DARK);
            myGrd.addColorStop(1, config.LINE_RED_LIGHT);
        }

        myBmp.context.fillStyle = myGrd;
        myBmp.context.fillRect(0, 0, w, h);

        const gradient = this.game.add.sprite(posX, posY, myBmp);
        gradient.angle = angle;

        const tween = this.game.add.tween(gradient).to({width: w}, 100, 'Linear', true);
        this.laser = gradient;
        tween.onComplete.add(callback)
    }

    removeLaser() {
        this.laser && this.laser.destroy();
    }

    freezeRobot() {
        soundManager.play('ice', false);
        this.freezeState = 2;
        this.loadTexture(this.frozenAsset);
        this.freezeAnim2.play(3, true);
    }

    freezeRobotLight() {
        this.freezeState = 1;
        this.loadTexture(this.frozenAsset);
        this.freezeAnim.play(3, true);
    }

    unfreezeRobot() {
        this.freezeState = 0;
        this.loadTexture(this.mainAsset);
    }

    animateRobot({x, y}, {callback}) {
        if (!this.moving) {
            this.moving = true;
            y = y - config.HEX_HEIGHT / 2;
            this.loadTexture(this.walkAsset);
            this.frame = 0;
            this.animations.play(this.walkAsset, 15, false);
            this.animations.currentAnim.onComplete.add(() => {
                this.loadTexture(this.mainAsset);
            });
            const margin = this.playerNumber === PLAYER_1 ? 12 : 0;
            y = y + margin;
            const tween = this.game.add.tween(this).to({x, y}, 600, Phaser.Easing.Linear.None, true);
            tween.onComplete.add(() => {
                this.moving = false;
                callback();
            })
        }
    }

    destroyRobot(data = {}) {
        const {animate, callback} = data;
        if (animate) {
            this.loadTexture(this.destroyAsset);
            this.frame = 0;
            this.animations.play(this.destroyAsset, 30);
            this.animations.currentAnim.onComplete.add(() => {
                this.destroy();
                this.shootIcon && this.shootIcon.destroy();
                callback && callback();
            });
        } else {
            this.destroy();
            this.shootIcon && this.shootIcon.destroy();
            callback && callback();
        }
    }

    enableShoot() {
        const icon = new ShootIcon({
            game: this.game,
            x: 0,
            y: 27,
            asset: `robot_${this.playerNumber === PLAYER_1 ? '1' : '2'}_shoot_icon`
        }, {
            clickCallback: () => {
                this.freezeState === 0 && this.shootCallback();
            }
        });
        centerGameObjects([icon]);
        icon.alpha = 1;
        this.shootIcon = icon;
        this.addChild(icon);
        this.addBonus();
    }

    enableObstacle() {
        const icon = new ObstacleIcon({
            game: this.game,
            x: 0,
            y: 27,
            asset: `robot_${this.playerNumber === PLAYER_1 ? '1' : '2'}_obstacle_icon`
        }, {
            clickCallback: () => {
                this.freezeState === 0 && this.obstacleCallback();
            }
        });
        centerGameObjects([icon]);
        icon.alpha = 1;
        this.obstacleIcon = icon;
        this.addChild(icon);
        this.addBonus();
    }

    enableFreeze() {
        const icon = new FreezeIcon({
            game: this.game,
            x: 0,
            y: 27,
            asset: `robot_${this.playerNumber === PLAYER_1 ? '1' : '2'}_freeze_icon`
        }, {
            clickCallback: () => {
                this.freezeState === 0 && this.freezeCallback();
            }
        });
        icon.alpha = 1;
        centerGameObjects([icon]);
        this.freezeIcon = icon;
        this.addChild(icon);
        this.addBonus();
    }

    removeShootSkill() {
        this.shootIcon.destroy();
        this.shootIcon = null;
        this.removeBonus();
    }

    removeFreezeSkill() {
        this.freezeIcon.destroy();
        this.freezeIcon = null;
        this.removeBonus();
    }

    removeObstacleSkill() {
        this.obstacleIcon.destroy();
        this.obstacleIcon = null;
        this.removeBonus();
    }
}
