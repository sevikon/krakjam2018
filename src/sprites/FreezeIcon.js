import Phaser from 'phaser'

export default class extends Phaser.Sprite {

    constructor({game, x, y, asset},{clickCallback}) {
        super(game, x, y, asset);
        this.anchor.set(0, 0);
        this.inputEnabled = true;
        this.events.onInputDown.add(clickCallback, this);
    }
}
