import Phaser from 'phaser';
import config from "../config";

const {PLAYER_1} = config;

export default class extends Phaser.Sprite {

    constructor({game, x, y, asset}, {clickCallback, playerNumber}) {
        super(game, x, y, asset);
        this.anchor.set(0, 0);
        this.inputEnabled = true;
        this.playerNumber = playerNumber;
        this.events.onInputDown.add(clickCallback, this);
        this.mainAsset = asset;
        this.takeoverAsset = `transmitter_${playerNumber === PLAYER_1 ? '1' : '2'}_takeover`;
        this.roundAsset = `transmitter_${playerNumber === PLAYER_1 ? '1' : '2'}_round`;

        this.loadTexture(this.takeoverAsset);
        this.animations.add(this.takeoverAsset);

        this.loadTexture(this.mainAsset);

        this.loadTexture(this.roundAsset);
        this.roundAnim = this.animations.add(this.roundAsset);

        this.scale.set(0.8, 0.8);
    }

    playTakeoverAnimation(animFinishedCallback) {
        this.loadTexture(this.takeoverAsset);
        this.frame = 0;
        this.animations.play(this.takeoverAsset, 25, false);
        this.animations.currentAnim.onComplete.add(animFinishedCallback);
    }

    playRoundAnimation(animFinishedCallback) {
        this.loadTexture(this.roundAsset);
        this.roundAnim.frame = 0;
        this.roundAnim.play(12, false);
        animFinishedCallback && this.animations.currentAnim.onComplete.add(animFinishedCallback);
    }

    stopRound() {
        this.roundAnim.stop();
    }
}
