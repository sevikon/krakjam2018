import Phaser from 'phaser'

export default class extends Phaser.Sprite {

    constructor({game, x, y, asset}) {
        super(game, x, y, asset);
        this.game = game;
        this.kind = asset;
        this.anchor.setTo(0.5, 0.5);
    }

    remove() {
        this.game.add.tween(this).to({alpha: 0}, 400, 'Linear', true);
        this.game.time.events.add(Phaser.Timer.SECOND * 0.5, this.destroy, this);
    }
}
