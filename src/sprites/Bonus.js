import Phaser from 'phaser'

export default class extends Phaser.Sprite {

    constructor({game, x, y}, {kind}) {
        super(game, x, y);
        this.anchor.set(0, 0);
        this.kind = kind;
        
        this.loadTexture(`bonus_${kind}`);
        this.animations.add(`bonus_${kind}`);
        this.animations.play(`bonus_${kind}`, 12, false);
    }
}
