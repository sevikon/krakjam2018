import config from '../config';
import Background from '../sprites/Background';
import soundManager from '../instances/SoundManager';

export default class GameEnd {

    init(conf) {
        this.animate = true;
        if (conf && conf.hasOwnProperty('animate')) {
            this.animate = conf.animate;
        }
        this.soundOn = true;
        this.win = 'win_pink';
        if (conf && conf.winner === config.PLAYER_1) {
            this.win = 'win_blue';
        }
    }

    create() {
        soundManager.stop('music', true);
        soundManager.play('winner', true);
        const w = game.width;
        const h = game.height;

        const background = new Background({
            game,
            asset: this.win
        });
        this.game.add.existing(background);

        //Przejście do menu
        const menuButton = game.add.button(0, 0, 'btn_menu', this.goMenu, this, 1, 0, 2);
        menuButton.x = w - menuButton.width - 10;
        menuButton.y = h - menuButton.height - 10;

        //Powtórz rozgrywkę
        const oneMore = game.add.button(0, 0, 'one_more', this.startGame, this, 1, 0, 2);
        oneMore.x = 20;
        oneMore.y = h - oneMore.height - 10;

        const soundButtonOn = game.add.button(0, 0, 'muzyka_on', this.toggleSound, this, 1, 0, 2);
        soundButtonOn.x = w - soundButtonOn.width - 20;
        soundButtonOn.y = 20;
        this.soundButtonOn = soundButtonOn;

        const soundButtonOff = game.add.button(0, 0, 'muzyka_off', this.toggleSound, this, 1, 0, 2);
        soundButtonOff.x = w - soundButtonOff.width - 20;
        soundButtonOff.y = 20;
        soundButtonOff.visible = false;
        this.soundButtonOff = soundButtonOff;


    }

    goMenu() {
        game.state.start('GameMenu', true, false);
        soundManager.stop('winner', true);

    }

    startGame() {
        game.state.start('Game', true, false);
        soundManager.stop('menu', true);
        soundManager.stop('winner', true);
        soundManager.play('music', true);
    }

    toggleSound() {
        this.soundOn = !this.soundOn;
        this.soundButtonOn.visible = this.soundOn;
        this.soundButtonOff.visible = !this.soundOn;
    }
}
