import Phaser from 'phaser'
import BoardSection from '../instances/BoardSection';
import Timer from '../instances/Timer';
import config from '../config';
import Background from '../sprites/Background';
import PlayerStorage from '../storage/PlayerStorage';

export default class extends Phaser.State {

    init() {

    }

    create() {

        //stałe do obiektów this.animals, this.game, this.points - żeby nie treba było pisać this.
        const {game} = this;

        const background = new Background({
            game,
            asset: 'background'
        });
        this.game.add.existing(background);

        this.playerStorage = new PlayerStorage({
            updateRobots: (player) => {
                this.updateRobots(player)
            },
            addBonusTime: (player) => {
                this.addBonusTime(player)
            },
            insertBonus: () => {
                this.insertBonus()
            },
            checkAvailableMoves: () => {
                this.checkAvailableMoves()
            }
        });

        this.board = new BoardSection({game}, {
            getActivePlayer: () => this.playerStorage.getActivePlayer(),
            playerStartedMove: (player) => this.playerStorage.playerStartedMove(player),
            playerFinishedMove: (player) => this.playerStorage.playerFinishedMove(player),
            togglePlayerBcNoMove: () => this.playerStorage.togglePlayerBcNoMove(),
        });
        this.game.time.events.loop(Phaser.Timer.SECOND, this.updateCounter, this);
        this.timer = new Timer({game}, {
            timer: config.GAME_LENGTH,
            callback: this.endGame,
            getActivePlayer: () => (this.playerStorage.getActivePlayer())
        });

    }

    updateRobots(player) {
        this.board.updateFrozenRobots(player)
    }

    addBonusTime(player) {
        this.timer.addBonusTime(player);
    }

    insertBonus() {
        this.board.insertBonus()
    }

    checkAvailableMoves() {
        this.board.checkAvailableMoves()
    }

    updateCounter() {
        this.timer.updateTimer();
    }

    endGame(player) {
        this.game.state.start('GameEnd', true, false, {winner: player});
    }

}
