import Phaser from 'phaser'
import WebFont from 'webfontloader'
import {centerGameObjects} from '../utils';

const assetPath = './assets/';
const assetPathMenu = assetPath + 'images/menu/';
const assetPathGame = assetPath + 'images/game/';
// const assetPathLevels = assetPath + 'images/levels/';
// const assetPathLevelDetails = assetPath + 'images/level_details/';

export default class extends Phaser.State {

    init() {
        this.stage.backgroundColor = '#EDEEC9';
    }

    preload() {

        WebFont.load({
            google: {
                families: ['Titillium Web']
            }
        });

        let text = this.add.text(this.world.centerX, this.world.centerY, 'this fonts', {
            font: '16px Arial',
            fill: '#dddddd',
            align: 'center'
        });
        // text.anchor.setTo(0.5, 0.5);
        this.load.image('loading_tlo', assetPathGame + 'loading_tlo.png');
        this.load.image('loading', assetPathGame + 'loading.png');
        this.load.image('progressBar', assetPathGame + 'progress_bar.png');
        this.load.image('progressBarFill', assetPathGame + 'progress_bar_fill.png');
    }

    create() {
        this.stage.disableVisibilityChange = true;
        this.load.onFileComplete.add(this.fileCompleteEvent, this);
        this.load.onLoadComplete.add(this.loadCompleteEvent, this);

        //Dodanie tła ładowania
        let position = {x: 40, y: 400};
        const back = this.add.sprite(0, 0, 'loading_tlo');
        back.width = this.game.width;
        back.height = this.game.height;

        //Dodanie paska ładowania
        const loadingBar_robot = this.add.sprite(position.x + 180, position.y - 70, 'progressBar');
        // loadingBar_robot.x = 
        // loadingBar_robot.y =

        //Dodanie postępu paska ładowania
        const loadingBar = this.menuItem = this.add.sprite(position.x, position.y, 'progressBarFill');

        loadingBar.x = 500;
        loadingBar.y = 850;

        //Napis loading
        this.add.sprite(position.x + 800, position.y + 550, 'loading');

        this.cropRect = new Phaser.Rectangle(0, 0, 0, this.menuItem.height);
        this.menuItem.crop(this.cropRect);

        this.loadStart();

        this.game.scale.scaleMode = Phaser.ScaleManager.SHOW_ALL;
        this.game.scale.pageAlignHorizontally = true;
        this.game.scale.pageAlignVertically = true;

        // const clientWidth = Math.max(window.innerWidth, document.documentElement.clientWidth);

        // if (clientWidth < 800) {
        //     this.game.scale.startFullScreen();
        // }
    }

    //Zwiększanie się paska ładowania
    fileCompleteEvent(progress, cacheKey, success, totalLoaded, totalFiles) {
        this.cropRect.width = progress * this.menuItem._frame.width / 100;
        this.menuItem.updateCrop();
    }

    //Ustawia początkowy stan gry
    loadCompleteEvent() {
        this.state.start('GameMenu', true, false, {
            winner: false
        });
    }

    loadStart() {
        //Instrukcja
        this.load.image('i_1', assetPathGame + `i_1.png`);
        this.load.image('i_2', assetPathGame + `i_2.png`);
        this.load.image('i_3', assetPathGame + `i_3.png`);
        this.load.image('i_next', assetPathGame + `i_Arrow.png`);
        this.load.image('i_skip', assetPathGame + `i_skip.png`);
        this.load.image('i_play', assetPathGame + `i_play.png`);
        this.load.image('i_previous', assetPathGame + `i_previous.png`);

        this.load.image('background', assetPathGame + `bg.png`);
        this.load.image('robot_1', assetPathGame + `robot_1.png`);
        this.load.image('robot_2', assetPathGame + `robot_2.png`);

        this.load.image('btn_play', assetPathMenu + 'btn_play.png');

        this.load.image('btn_info', assetPathMenu + 'info.png');
        // this.load.image('music_on', assetPathMenu + 'music_on.png');
        // this.load.image('music_off', assetPathMenu + 'music_off.png');

        this.load.image('hex', assetPathGame + `hex_1.png`);
        this.load.image('hex_2', assetPathGame + `hex_2.png`);
        this.load.image('hex_3', assetPathGame + `hex_3.png`);
        this.load.image('hex_4', assetPathGame + `hex_4.png`);
        this.load.image('hex_5', assetPathGame + `hex_5.png`);
        this.load.image('hex_6', assetPathGame + `hex_6.png`);
        this.load.image('hex_7', assetPathGame + `hex_7.png`);
        this.load.image('hex_8', assetPathGame + `hex_8.png`);

        // this.load.image('transmitter_1', assetPathGame + `antena_n.png`);
        // this.load.image('transmitter_2', assetPathGame + `antena_c.png`);

        this.load.image('transmitter_1', assetPathGame + `antena_n_1.png`);
        this.load.image('transmitter_2', assetPathGame + `antena_c_1.png`);
        this.load.image('transmitter_1_hex', assetPathGame + `antena_n_2.png`);
        this.load.image('transmitter_2_hex', assetPathGame + `antena_c_2.png`);

        this.load.image('robot_1_shoot_icon', assetPathGame + `robot_1_shoot_icon.png`);
        this.load.image('robot_1_obstacle_icon', assetPathGame + `robot_1_obstacle_icon.png`);
        this.load.image('robot_1_speed_icon', assetPathGame + `robot_1_speed_icon.png`);
        this.load.image('robot_1_freeze_icon', assetPathGame + `robot_1_freeze_icon.png`);

        this.load.image('robot_2_shoot_icon', assetPathGame + `robot_2_shoot_icon.png`);
        this.load.image('robot_2_obstacle_icon', assetPathGame + `robot_2_obstacle_icon.png`);
        this.load.image('robot_2_speed_icon', assetPathGame + `robot_2_speed_icon.png`);
        this.load.image('robot_2_freeze_icon', assetPathGame + `robot_2_freeze_icon.png`);

        this.load.spritesheet('bonus_freeze', assetPathGame + 'bonus_freeze.png', 105, 120, 4);
        this.load.spritesheet('bonus_shoot', assetPathGame + 'bonus_shoot.png', 105, 120, 4);
        this.load.spritesheet('bonus_obstacle', assetPathGame + 'bonus_obstacle.png', 105, 120, 4);
        this.load.spritesheet('bonus_speed', assetPathGame + 'bonus_speed.png', 105, 120, 4);

        this.load.image('win_pink', assetPathGame + `game_over_c.png`);
        this.load.image('win_blue', assetPathGame + `game_over_n.png`);

        this.load.image('bg_menu', assetPathGame + `bg_menu.png`);
        this.load.image('one_more', assetPathGame + `one_more.png`);
        this.load.image('bnt_play', assetPathGame + `bnt_play.png`);
        this.load.image('bnt_instrukcja', assetPathGame + `bnt_instrukcja.png`);

        this.load.image('btn_menu', assetPathGame + 'btn_menu.png');
        this.load.image('muzyka_on', assetPathGame + 'muzyka_on.png');
        this.load.image('muzyka_off', assetPathGame + 'muzyka_off.png');

        this.load.spritesheet('robot_1_walk', assetPathGame + 'robot_1_idzie.png', 95, 160, 8);
        this.load.spritesheet('robot_2_walk', assetPathGame + 'robot_2_idzie.png', 75, 170, 8);

        this.load.spritesheet('transmitter_1_takeover', assetPathGame + 'antena_1_animacja.png', 145, 250, 38);
        this.load.spritesheet('transmitter_2_takeover', assetPathGame + 'antena_2_animacja.png', 145, 250, 38);

        this.load.spritesheet('transmitter_1_round', assetPathGame + 'antena_1_obrot.png', 145, 250, 20);
        this.load.spritesheet('transmitter_2_round', assetPathGame + 'antena_2_obrot.png', 145, 250, 20);

        this.load.spritesheet('robot_1_explosion', assetPathGame + 'robot_1_wybuch.png', 140, 185, 15);
        this.load.spritesheet('robot_2_explosion', assetPathGame + 'robot_2_wybuch.png', 140, 170, 14);

        this.load.spritesheet('robot_1_frozen', assetPathGame + 'robot_1_zamrozenie.png', 105, 160, 6);
        this.load.spritesheet('robot_2_frozen', assetPathGame + 'robot_2_zamrozenie.png', 85, 170, 6);

        this.load.spritesheet('player_menu_1', assetPathGame + 'podstawka_n.png');
        this.load.spritesheet('player_menu_2', assetPathGame + 'podstawka_c.png');

        //Dźwięki
        this.load.audio('music', ['assets/sounds/battle.mp3']);
        this.load.audio('menu', ['assets/sounds/menu.mp3']);
        this.load.audio('laser', ['assets/sounds/laser.mp3']);
        this.load.audio('select', ['assets/sounds/select.mp3']);
        this.load.audio('appear', ['assets/sounds/appear.mp3']);
        this.load.audio('bum', ['assets/sounds/bum.mp3']);
        this.load.audio('winner', ['assets/sounds/winner.mp3']);
        this.load.audio('ice', ['assets/sounds/ice.mp3']);

        this.load.start();
    }

}
