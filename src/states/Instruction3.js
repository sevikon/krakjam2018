import Background from '../sprites/Background';
import soundManager from '../instances/SoundManager';
import menuMusicManager from "../instances/MenuMusicManager";

export default class Instruction3 {

    init(conf) {
        this.animate = true;
        if (conf && conf.hasOwnProperty('animate')) {
            this.animate = conf.animate;
        }
        this.soundOn = true;
    }

    create() {
        const w = game.width;
        const h = game.height;

        //Obrazek tła
        let picture = 'i_3';

        const background = new Background({
            game,
            asset: picture
        });
        this.game.add.existing(background);

        //Play
        const playButton = game.add.button(0, 0, 'i_play', this.startGame, this, 1, 0, 2);
        playButton.x = w / 2 - playButton.width / 2;
        playButton.y = h / 2 + 385;

        //Przycisk przejścia do Instrukcji2
        const previousButton = game.add.button(0, 0, 'i_previous', this.Instruction2, this, 1, 0, 2);
        previousButton.x = 20;
        previousButton.y = h / 2 - previousButton.height / 2;
        this.previousButton = previousButton;

        //Przyciski włączania/wyłączania muzyki
        const soundButtonOn = game.add.button(0, 0, 'muzyka_on', this.toggleSound, this, 1, 0, 2);
        soundButtonOn.x = w - soundButtonOn.width - 20;
        soundButtonOn.y = 20;
        this.soundButtonOn = soundButtonOn;

        const soundButtonOff = game.add.button(0, 0, 'muzyka_off', this.toggleSound, this, 1, 0, 2);
        soundButtonOff.x = w - soundButtonOff.width - 20;
        soundButtonOff.y = 20;
        soundButtonOff.visible = false;
        this.soundButtonOff = soundButtonOff;

        this.checkButtonVisible();
    }

    startGame() {
        game.state.start('Game', true, false);
        soundManager.stop('menu', true);
        soundManager.play('music', true);
    }

    Instruction2() {
        game.state.start('Instruction2', true, false);
    }

    toggleSound() {
        menuMusicManager.toggleSound();
        this.checkButtonVisible();
    }

    checkButtonVisible() {
        this.soundButtonOn.visible = menuMusicManager.isPlaying();
        this.soundButtonOff.visible = !menuMusicManager.isPlaying();
    }
}
