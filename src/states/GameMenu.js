/* globals game */
import Phaser from 'phaser';
import soundManager from '../instances/SoundManager';
import menuMusicManager from '../instances/MenuMusicManager';

export default class Menu {

    init(conf) {
        this.animate = true;
        if (conf && conf.hasOwnProperty('animate')) {
            this.animate = conf.animate;
        }
    }

    create() {
        const w = game.width;
        const h = game.height;


        //Obrazek tła menu
        const back = game.add.image(0, 0, 'bg_menu');

        back.width = w;
        back.height = h;

        const menuGroup = game.add.group();

        //Przycisk "PLAY" prowadzący do pierwszego ekranu instrukcji
        const playButton = game.add.button(0, 0, 'bnt_play', this.Instruction1, this, 1, 0, 2);
        playButton.x = w / 2 - playButton.width / 2;
        playButton.y = h / 2 + 150;

        //Przyciski włączania/wyłączania muzyki
        const soundButtonOn = game.add.button(0, 0, 'muzyka_on', this.toggleSound, this, 1, 0, 2);
        soundButtonOn.x = w - soundButtonOn.width - 20;
        soundButtonOn.y = 20;
        this.soundButtonOn = soundButtonOn;

        const soundButtonOff = game.add.button(0, 0, 'muzyka_off', this.toggleSound, this, 1, 0, 2);
        soundButtonOff.x = w - soundButtonOff.width - 20;
        soundButtonOff.y = 20;
        soundButtonOff.visible = false;
        this.soundButtonOff = soundButtonOff;

        //Pokazuje co wywołać jak klikniesz w przycisk "PLAY"
        this.game.input.keyboard.addKey(Phaser.Keyboard.ENTER).onDown.add(this.Instruction1);

        menuGroup.add(playButton);
        menuGroup.add(soundButtonOn);
        menuGroup.add(soundButtonOff);
        menuGroup.alpha = 0;

        const menuTween = game.add.tween(menuGroup);
        menuTween.to({
            alpha: 1
        }, 500);

        soundManager.add('laser');
        soundManager.add('select');
        soundManager.add('appear');
        soundManager.add('music');
        soundManager.add('menu');
        soundManager.add('bum');
        soundManager.add('winner');
        soundManager.add('ice');

        this.checkButtonVisible();
        menuMusicManager.startMusic();

        if (this.animate) {
            menuTween.onComplete.add(() => {
            });
            menuTween.start();
        } else {
            menuGroup.alpha = 1;
        }

    }

    toggleSound() {
        menuMusicManager.toggleSound();
        this.checkButtonVisible();
    }

    checkButtonVisible() {
        this.soundButtonOn.visible = menuMusicManager.isPlaying();
        this.soundButtonOff.visible = !menuMusicManager.isPlaying();
    }

    Instruction1() {
        game.state.start('Instruction1', true, false);

    }
}
