import Phaser from 'phaser';

const HEX_W = 6;
const HEX_H = 5;

export default {
    gameWidth: 1920,
    gameHeight: 1080,
    GAME_LENGTH: 180,
    SECONDS_ADDED_AFTER_MOVE: 10,
    HEX_W,
    HEX_H,
    HEX_WIDTH: 169,
    HEX_HEIGHT: 147,
    PADDING_X: 480,
    PADDING_Y: 260,
    ROBOT_MARGIN: 50,
    LEVEL_DETAILS_LIFETIME: 5,
    MOVES: 2,
    BONUS_INTERVAL: 5,
    initialPlayerVelocity: 360,
    TRANSMITTER_1_POS: {
        x: (HEX_H - 1) / 2 - 1,
        y: 0
    },
    TRANSMITTER_2_POS: {
        x: (HEX_H - 1) / 2 + 1,
        y: HEX_W
    },
    LINE_BLUE_DARK: '#3399CC',
    LINE_BLUE_LIGHT: '#95d4f3',

    LINE_RED_DARK: '#611630',
    LINE_RED_LIGHT: '#e0a2b6',

    PLAYER_1: 'PLAYER_1',
    PLAYER_2: 'PLAYER_2',
    ENEMIES_COLS_NUM: 2,
    localStorageName: 'krakjam2018',
    assetsPath: 'assets',
    imgPath: 'assets/images',
    tilesPath: 'assets/tilemaps',
    spritesheetsPath: 'assets/spritesheets',
    soundsPath: 'assets/sounds',
    physicsType: Phaser.Physics.ARCADE,
    BONUS_KIND_SPEED: 'speed',
    BONUS_KIND_OVER_VOLTAGE: 'over_voltage',
    BONUS_KIND_OBSTACLE: 'obstacle',
    BONUS_KIND_SHOOT: 'shoot',
    BONUS_KIND_FREEZE: 'freeze',
    textStyle: {
        font: '26px Lobster Two',
        fontWeight: 'bold',
        style: 'italic',
        fill: '#ffffff',
        align: 'center',
        stroke: '#000',
        strokeThickness: 5
    },
    largeTextStyle: {
        font: '52px Lobster Two',
        fontWeight: 'bold',
        style: 'italic',
        fill: '#ffffff',
        align: 'center',
        stroke: '#000',
        strokeThickness: 5
    },
    player1: {
        keys: {
            left: Phaser.Keyboard.A,
            right: Phaser.Keyboard.D,
            up: Phaser.Keyboard.W,
            down: Phaser.Keyboard.S,
            placeSack: Phaser.Keyboard.V,
            destroySack: Phaser.Keyboard.B

        }
    },
    player2: {
        keys: {
            left: Phaser.Keyboard.LEFT,
            right: Phaser.Keyboard.RIGHT,
            up: Phaser.Keyboard.UP,
            down: Phaser.Keyboard.DOWN,
            placeSack: Phaser.Keyboard.COLON,
            destroySack: Phaser.Keyboard.QUOTES
        }
    },

    game: {
        keys: {
            esc: Phaser.Keyboard.ESC
        }
    }
};
