/* globals game */
import Phaser from 'phaser';

export default class SoundManager {

    constructor() {
        this.sounds = [];
    }

    add(name) {
        this.sounds[name] = game.add.audio(name);

        /**
         * This event will occur every time sound finished playing
         * @type {Phaser.Signal}
         */
        this.sounds[name].onEnd = new Phaser.Signal();
    }

    load(name, arr) {
        game.load.audio(name, arr);
    }

    play(name, loop) {
        this.sounds[name].active = true;
        this.sounds[name].loop = !!loop;
        this.sounds[name].paused = false;
        this.sounds[name].play();
    }

    restart(name, marker, position, volume, loop) {
        this.sounds[name].restart(marker, position, volume, loop);
    }

    refresh(name) {
        this.sounds[name].pause();
        this.sounds[name].resume();
    }

    pause(name) {
        this.sounds[name].pause();
    }

    resume(name) {
        this.sounds[name].resume();
    }

    stop(name) {
        this.sounds[name].active = false;
        this.sounds[name].stop();
    }

    get(name) {
        return this.sounds[name];
    }

    remove(name) {
        game.sound.remove(this.sounds[name]);
        delete this.sounds[name];
    }

    flagAsMultiple(name) {
        this.sounds[name].allowMultiple = true;
    }

    seek(key, positionInMS) {
        const s = this.sounds[key];

        if (!s.paused) {
            s.pause();
            s.pausedPosition = positionInMS;
            s.currentTime = positionInMS;
            s.startTime = Math.max(0, s.startTime - positionInMS);
            s.resume();
        } else {
            s.pausedPosition = positionInMS;
            s.currentTime = positionInMS;
            s.startTime = Math.max(0, s.startTime - positionInMS);
        }

    }

    pauseAll() {
        const soundKeys = Object.keys(this.sounds);

        for (let i = 0; i < soundKeys.length; ++i)
            this.pause(soundKeys[i]);
    }

    stopAll() {
        const soundKeys = Object.keys(this.sounds);

        for (let i = 0; i < soundKeys.length; ++i)
            this.stop(soundKeys[i]);
    }

    setPlaybackRate(name, playbackRate) {
        if (this.sounds[name].isPlaying)
            this.sounds[name]._sound.playbackRate.value = playbackRate;
    }

    setGlobalPlaybackRate(playbackRate) {
        const soundKeys = Object.keys(this.sounds);

        for (let i = 0; i < soundKeys.length; ++i)
            this.setPlaybackRate(soundKeys[i], playbackRate);
    }
}