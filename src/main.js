import 'pixi'
import 'p2'
import Phaser from 'phaser'

import BootState from './states/Boot'
import GameState from './states/Game'
import GameMenuState from './states/GameMenu'
import Instruction1State from './states/Instruction1'
import Instruction2State from './states/Instruction2'
import Instruction3State from './states/Instruction3'
import GameEnd from './states/GameEnd'

import config from './config'

class Game extends Phaser.Game {
    constructor() {
        // const docElement = document.documentElement;
        // const width = docElement.clientWidth > config.gameWidth ? config.gameWidth : docElement.clientWidth;
        // const height = docElement.clientHeight > config.gameHeight ? config.gameHeight : docElement.clientHeight;

        super(config.gameWidth, config.gameHeight, Phaser.CANVAS, 'content', null);

        this.state.add('Boot', BootState, false);
        this.state.add('Instruction1', Instruction1State, false);
        this.state.add('Instruction2', Instruction2State, false);
        this.state.add('Instruction3', Instruction3State, false);
        this.state.add('Game', GameState, false);
        this.state.add('GameEnd', GameEnd, false);
        this.state.add('GameMenu', GameMenuState, false);

        // with Cordova with need to wait that the device is ready so we will call the Boot state in another file
        if (!window.cordova) {
            this.state.start('Boot')
        }
    }
}

window.game = new Game();

if (window.cordova) {
    const app = {
        initialize: function () {
            document.addEventListener(
                'deviceready',
                this.onDeviceReady.bind(this),
                false
            )
        },

        // deviceready Event Handler
        //
        onDeviceReady: function () {
            this.receivedEvent('deviceready');

            // When the device is ready, start Phaser Boot state.
            window.game.state.start('Boot')
        },

        receivedEvent: function (id) {
            console.log('Received Event: ' + id)
        }
    };

    app.initialize()
}
