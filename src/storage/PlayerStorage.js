import config from '../config';

export default class PlayerStorage {

    constructor({updateRobots, addBonusTime, insertBonus, checkAvailableMoves}) {
        this.activePlayer = null;
        this.updateRobots = updateRobots;
        this.addBonusTime = addBonusTime;
        this.checkAvailableMoves = checkAvailableMoves;
        this.insertBonus = insertBonus;
        this.moves = config.MOVES;
        this.bonusCnt = config.BONUS_INTERVAL;
    }

    playerStartedMove(player) {
        if (player !== this.activePlayer) {
            this.activePlayer = player;
        }
    }

    playerFinishedMove(player) {
        if (player === this.activePlayer) {
            this.moves--;
            this.bonusCnt--;

            if (this.moves <= 0) {
                this.changePlayer(player);
            }

            if (this.bonusCnt <= 0) {
                this.bonusCnt = config.BONUS_INTERVAL;
                this.insertBonus();
            }
            this.checkAvailableMoves();
        }
    }


    changePlayer(player) {
        const curr = player === config.PLAYER_1 ? config.PLAYER_2 : config.PLAYER_1;
        this.moves = config.MOVES;
        this.activePlayer = curr;
        this.updateRobots(player);
        this.addBonusTime(player);
    }

    getActivePlayer() {
        return this.activePlayer;
    }

    togglePlayerBcNoMove() {
        this.changePlayer(this.activePlayer);
    }
}