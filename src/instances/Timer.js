import config from '../config';
import PlayerMenu from "../sprites/PlayerMenu";

const ALPHA_HIDDEN = 0.3;

export default class Timer {

    constructor({game}, {timer = 0, callback, getActivePlayer}) {
        this.game = game;
        this.callback = callback;
        const padding = 25;
        this.player_1_menu = this.addPlayerMenu(0, padding, 'player_menu_1');
        this.player_1 = this.addPlayerTimer(30, padding);
        this.player_2_menu = this.addPlayerMenu(this.game.world.width, this.game.world.height - padding, 'player_menu_2');
        this.player_2 = this.addPlayerTimer(this.game.world.width - 160 - padding, this.game.world.height - 170 - padding);
        this.timer_1 = timer;
        this.timer_2 = timer;
        this.getActivePlayer = getActivePlayer;
        this.updateTimer();
    }

    addPlayerMenu(x, y, asset) {
        const icon = new PlayerMenu({
            game: this.game,
            x,
            y,
            asset
        });
        // centerGameObjects([icon]);
        icon.alpha = 1;
        if (asset === 'player_menu_2') {
            icon.anchor.setTo(1, 1);
        }
        this.game.add.existing(icon);
        return icon;
    }

    addPlayerTimer(x, y) {
        const text = this.game.add.text(x + 80, y + 80, '', {
            font: '70px Titillium Web',
            fill: '#ffffff',
            align: 'center'
        });
        text.anchor.setTo(0.5, 0.5);
        return text;
    }

    updateTimer() {
        const activePlayer = this.getActivePlayer();
        if (activePlayer === config.PLAYER_1) {
            this.player_1.alpha = 1;
            this.player_1_menu.alpha = 1;
            this.player_2.alpha = ALPHA_HIDDEN;
            this.player_2_menu.alpha = ALPHA_HIDDEN;
            this.timer_1 > 0 && this.timer_1--;
        } else if (activePlayer === config.PLAYER_2) {
            this.player_1.alpha = ALPHA_HIDDEN;
            this.player_1_menu.alpha = ALPHA_HIDDEN;
            this.player_2.alpha = 1;
            this.player_2_menu.alpha = 1;
            this.timer_2 > 0 && this.timer_2--;
        }
        this.player_1.setText(this.updateUserTimer(this.timer_1));
        this.player_2.setText(this.updateUserTimer(this.timer_2));

        if (this.timer_1 <= 0) {
            this.callback({
                player: config.PLAYER_1
            });
        } else if (this.timer_2 <= 0) {
            this.callback({
                player: config.PLAYER_2
            });
        }
    }

    updateUserTimer(seconds) {
        const min = Math.floor(seconds / 60);
        const sec = seconds % 60;
        return `${Timer.round(min)}:${Timer.round(sec)}`;
    }

    addBonusTime(player) {
        if (this.getActivePlayer() === config.PLAYER_1) {
            this.timer_2 += config.SECONDS_ADDED_AFTER_MOVE;
        } else {
            this.timer_1 += config.SECONDS_ADDED_AFTER_MOVE;
        }

        this.showBonusTimeText(player)
    }

    showBonusTimeText(player) {
        const activePlayer = this.getActivePlayer();

        const playerMenu = activePlayer === config.PLAYER_1 ? this.player_2_menu : this.player_1_menu
        const horPadding = activePlayer === config.PLAYER_1 ? -180 : +50
        const verPadding = activePlayer === config.PLAYER_1 ? -200 : +100

        const text = this.game.add.text(playerMenu.x + horPadding, playerMenu.y + verPadding, '+' + config.SECONDS_ADDED_AFTER_MOVE, {
            font: '70px Titillium Web',
            fill: '#a0d0a0',
            align: 'center'
        });

        const tween = this.game.add.tween(text).to(
            {
                y: activePlayer === config.PLAYER_1 ? (text.y - 200) : (text.y + 200),
                alpha: 0
            },
            1000,
            Phaser.Easing.Linear.None,
            true
        );

        tween.onComplete.add(() => {
            text.destroy();
        });
    }

    static round(value) {
        return value <= 9 ? '0' + value : value
    }
}
