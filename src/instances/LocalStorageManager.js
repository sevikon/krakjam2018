import soundManager from "../instances/SoundManager";
import store from 'store';

class LocalStorageManager {

    init() {
        this.store = store;
    }

    get(key) {
        return this.store.get(key);
    }

    set(key, value) {
        this.store.set(key, value);
    }

}

const manager = new LocalStorageManager();
manager.init();
export default manager;
