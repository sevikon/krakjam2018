import config from '../config';
import Hex from '../sprites/Hex';
import {centerGameObjects} from '../utils';
import soundManager from '../instances/SoundManager';
import {Timer} from 'phaser';

const {
    HEX_W, HEX_H, HEX_WIDTH, HEX_HEIGHT, PADDING_X, PADDING_Y, PLAYER_1, PLAYER_2,
    TRANSMITTER_1_POS, TRANSMITTER_2_POS, ENEMIES_COLS_NUM, BONUS_KIND_SHOOT, BONUS_KIND_FREEZE, BONUS_KIND_SPEED, BONUS_KIND_OBSTACLE
} = config;

export default class BoardSection {

    constructor({game}, {playerStartedMove, playerFinishedMove, getActivePlayer, togglePlayerBcNoMove}) {
        this.game = game;
        this.grid = [];
        this.playerStartedMove = playerStartedMove;
        this.playerFinishedMove = playerFinishedMove;
        this.playerFinishedMoveCallback = playerFinishedMove;
        this.togglePlayerBcNoMove = togglePlayerBcNoMove;
        this.getActivePlayer = getActivePlayer;
        this.waiting = false;

        for (let a = 0; a < HEX_H; a++) {
            this.grid[a] = [];
            const odd = a % 2 === 1;
            for (let b = 0; b < HEX_W || (b <= HEX_W && odd); b++) {
                let padding = a % 2 === 0 ? 0 : -HEX_WIDTH / 2;

                let type = 'normal';
                if (this.isTransmitter1AtCoords(a, b)) {
                    type = 'blue';
                }
                else if (this.isTransmitter2AtCoords(a, b)) {
                    type = 'pink';
                }
                const hex = this.addHex({
                    x: PADDING_X + padding + b * HEX_WIDTH,
                    y: PADDING_Y + a * HEX_HEIGHT
                }, {
                    type: type,
                    idx: b,
                    idy: a,
                    shootCallback: ({idx, idy}) => this.handleShootClick({idx, idy}),
                    clickCallback: ({idx, idy}) => this.handleHexClick({idx, idy}),
                    obstacleCallback: ({idx, idy}) => this.handleObstacleClick({idx, idy}),
                    freezeCallback: ({idx, idy}) => this.handleFreezeClick({idx, idy})
                });

                this.grid[a][b] = hex;
                centerGameObjects([hex]);
            }
        }

        for (let a = 0; a < HEX_H; a++) {

            const odd = a % 2 === 1;
            for (let b = 0; b < HEX_W || (b <= HEX_W && odd); b++) {

                if (a > 0 && a < HEX_H - 1) {
                    if (b < ENEMIES_COLS_NUM && (a !== TRANSMITTER_1_POS.x || b !== TRANSMITTER_1_POS.y)) {
                        this.grid[a][b].addRobot({
                            playerNumber: PLAYER_1
                        });
                        if (b === 0 && a === 2) {
                            this.grid[a][b].robot.enableShoot();
                        }
                        if (b === 1 && a === 3) {
                            this.grid[a][b].robot.enableFreeze();
                        }
                    }

                    if (
                        b > HEX_W - ENEMIES_COLS_NUM - (a % 2 === 1 ? 0 : 1) &&
                        (a !== TRANSMITTER_2_POS.x || b !== TRANSMITTER_2_POS.y)
                    ) {
                        this.grid[a][b].addRobot({
                            playerNumber: PLAYER_2
                        });
                        if (b === HEX_W - 1 && a === 2) {
                            this.grid[a][b].robot.enableShoot();
                        }
                        if (b === HEX_W - 1 && a === 1) {
                            this.grid[a][b].robot.enableFreeze();
                        }
                    }
                }
            }
        }

        // Dodanie wieży transmisyjnych
        this.trans_1 = this.grid[TRANSMITTER_1_POS.x][TRANSMITTER_1_POS.y].addTransmitter({
            playerNumber: PLAYER_1
        });
        this.trans_2 = this.grid[TRANSMITTER_2_POS.x][TRANSMITTER_2_POS.y].addTransmitter({
            playerNumber: PLAYER_2
        });

        this.game.time.events.loop(Timer.SECOND * 7, () => {
            this.trans_1.playRoundAnimation();
        }, this);

        this.game.time.events.loop(Timer.SECOND * 9, () => {
            this.trans_2.playRoundAnimation();
        }, this);

        // this.addBonus(2, 2, 0);
        // this.addBonus(3, 2, 1);
        // this.addBonus(4, 2, 2);

        this.activeHex = null;
    }

    updateFrozenRobots(player) {
        for (let a = 0; a < HEX_H; a++) {
            for (let b = 0; b <= HEX_W; b++) {
                if (this.grid[a][b] && this.grid[a][b].robot) {
                    const robot = this.grid[a][b].robot;
                    if (robot.playerNumber === player) {
                        if (robot.freezeState === 2) {
                            robot.freezeRobotLight();
                        } else if (robot.freezeState === 1) {
                            robot.unfreezeRobot();
                        }
                    }
                }
            }
        }
    }

    //Pojawienie się bonusa na planszy
    insertBonus() {
        soundManager.play('appear', false);
        while (true) {
            const y = Math.floor(Math.random() * HEX_H);
            const x = Math.floor(Math.random() * HEX_W) + (y % 2);

            if (
                this.grid[y][x].robot === null && this.grid[y][x].transmitter === null &&
                this.grid[y][x].state !== Hex.states.OBSTRUCTED
            ) {
                this.addBonus(y, x);
                break;
            }
        }
    }

    clear() {
        for (let a = 0; a < HEX_H; a++) {
            for (let b = 0; b <= HEX_W; b++) {
                if (this.grid[a][b]) {
                    this.grid[a][b].clear();
                }
            }
        }
    }

    markNeighboursEnterable({idx, idy}) {

        this.clear();

        const x = idx;
        const y = idy;

        this.activeHex = this.grid[y][x];

        const activePlayerNumber = this.activeHex.robot.playerNumber;
        this.playerStartedMove(activePlayerNumber);

        BoardSection.getNeighbours(x, y).map((item) => this.markHexEnterable(item[0], item[1], activePlayerNumber));
    }

    static getNeighbours(x, y) {
        return y % 2 === 0 ? [
            [x, y + 1],
            [x, y - 1],
            [x + 1, y - 1],
            [x + 1, y],
            [x - 1, y],
            [x + 1, y + 1]
        ] : [
            [x - 1, y - 1],
            [x, y - 1],
            [x - 1, y],
            [x + 1, y],
            [x - 1, y + 1],
            [x, y + 1]
        ];
    }

    addBonus(a, b, bonus) {
        const which = bonus !== undefined ? bonus : Math.floor(Math.random() * 3);
        let kind = null;

        switch (which) {
            case 0:
                kind = BONUS_KIND_OBSTACLE;
                break;

            case 1:
                kind = BONUS_KIND_FREEZE;
                break;

            case 2:
                kind = BONUS_KIND_SHOOT;
                break;
        }

        this.grid[a][b].addBonus({
            kind
        });
    }

    checkIconCanBeUsed({idx, idy}) {

        const clickedHex = this.grid[idy][idx];

        if (this.getActivePlayer() === clickedHex.robot.playerNumber) {
            return clickedHex.robot.playerNumber === this.activeHex.robot.playerNumber;
        } else {
            return false
        }
    }

    handleShootClick({idx, idy}) {
        this.handleHexClick({idx, idy});
        if (this.checkIconCanBeUsed({idx, idy})) {
            this.markNeighboursShootable({idx, idy});
        } else {
            this.clear();
        }
    }

    handleObstacleClick({idx, idy}) {
        this.handleHexClick({idx, idy});
        if (this.checkIconCanBeUsed({idx, idy})) {
            this.markNeighboursObstacable({idx, idy});
        }
    }

    handleFreezeClick({idx, idy}) {
        this.handleHexClick({idx, idy});
        if (this.checkIconCanBeUsed({idx, idy})) {
            this.markNeighboursFreezeable({idx, idy});
        }
    }

    markNeighboursShootable({idx, idy}) {

        this.clear();

        const x = idx;
        const y = idy;

        this.activeHex = this.grid[y][x];

        const activePlayerNumber = this.activeHex.robot.playerNumber;
        this.playerStartedMove(activePlayerNumber);

        BoardSection.getNeighbours(x, y).map((item) => BoardSection.getNeighbours(item[0], item[1]).map((item2) => this.markHexShootable(item2[0], item2[1], activePlayerNumber)));
    }

    markNeighboursObstacable({idx, idy}) {

        this.clear();

        const x = idx;
        const y = idy;

        this.activeHex = this.grid[y][x];

        const activePlayerNumber = this.activeHex.robot.playerNumber;
        this.playerStartedMove(activePlayerNumber);

        BoardSection.getNeighbours(x, y).map((item) => BoardSection.getNeighbours(item[0], item[1]).map((item2) => BoardSection.getNeighbours(item2[0], item2[1]).map((item3) => this.markHexObstacable(item3[0], item3[1]))));
    }

    markNeighboursFreezeable({idx, idy}) {

        this.clear();

        const x = idx;
        const y = idy;

        this.activeHex = this.grid[y][x];

        const activePlayerNumber = this.activeHex.robot.playerNumber;
        this.playerStartedMove(activePlayerNumber);

        BoardSection.getNeighbours(x, y).map((item) => BoardSection.getNeighbours(item[0], item[1]).map((item2) => BoardSection.getNeighbours(item2[0], item2[1]).map((item3) => this.markHexFreezeable(item3[0], item3[1], activePlayerNumber))));
    }

    markHexEnterable(x, y, forbiddenPlayerNumber) {
        if (
            this.grid[y] && this.grid[y][x] &&
            !(this.isTransmitter1AtCoords(y, x) && forbiddenPlayerNumber === PLAYER_1) &&
            !(this.isTransmitter2AtCoords(y, x) && forbiddenPlayerNumber === PLAYER_2) &&
            (this.grid[y][x].robot === null || this.grid[y][x].robot.playerNumber !== forbiddenPlayerNumber) &&
            this.grid[y][x].state !== Hex.states.OBSTRUCTED
        ) {
            this.grid[y][x].markEnterable(forbiddenPlayerNumber);
        }
    }

    markHexShootable(x, y, forbiddenPlayerNumber) {
        if (
            this.grid[y] && this.grid[y][x] &&
            !(this.isTransmitter1AtCoords(y, x) && forbiddenPlayerNumber === PLAYER_1) &&
            !(this.isTransmitter2AtCoords(y, x) && forbiddenPlayerNumber === PLAYER_2) &&
            (this.grid[y][x].robot === null || this.grid[y][x].robot.playerNumber !== forbiddenPlayerNumber)
        ) {
            this.grid[y][x].markShootable();
        }
    }

    markHexObstacable(x, y) {
        if (
            this.grid[y] && this.grid[y][x] &&
            !this.isTransmitter1AtCoords(y, x) &&
            !this.isTransmitter2AtCoords(y, x) &&
            this.grid[y][x].robot === null &&
            this.grid[y][x].state !== Hex.states.OBSTRUCTED
        ) {
            this.grid[y][x].markObstacable();
        }
    }

    markHexFreezeable(x, y, forbiddenPlayerNumber) {
        if (
            this.grid[y] && this.grid[y][x] &&
            !this.isTransmitter1AtCoords(y, x) &&
            !this.isTransmitter2AtCoords(y, x) &&
            (this.grid[y][x].robot === null || this.grid[y][x].robot.playerNumber !== forbiddenPlayerNumber)
        ) {
            this.grid[y][x].markFreezable();
        }
    }

    addHex({x, y}, data) {
        let obrazek = 'hex';
        if (data.type === 'pink') {
            obrazek = 'transmitter_2_hex'
        } else if (data.type === 'blue') {
            obrazek = 'transmitter_1_hex'
        }
        const hex = new Hex({
            game: this.game,
            x,
            y,
            asset: obrazek
        }, data);
        this.game.add.existing(hex);
        return hex;
    }

    endMove() {
        this.waiting = false;
    }

    handleHexClick({idx, idy}) {

        if (this.waiting) {
            return false;
        }
        this.waiting = true;

        const clickedHex = this.grid[idy][idx];
        if (clickedHex.state === Hex.states.CLEAR) {
            if (clickedHex.robot !== null &&
                (this.getActivePlayer() === clickedHex.robot.playerNumber || this.getActivePlayer() === null)
            ) {
                if (clickedHex.robot.freezeState > 0) {
                    this.clear();
                } else {
                    if (clickedHex.bonus) {
                        clickedHex.collectBonus();
                    }
                    this.markNeighboursEnterable({idx, idy});
                }
            } else {
                this.clear();
            }
            this.endMove();
        } else if (clickedHex.state === Hex.states.ENTERABLE) {

            // if we've clicked field with enemy transmitter
            if (clickedHex.transmitter !== null) {
                this.activeHex.robot.drawLaser({x: clickedHex.x, y: clickedHex.y}, {
                    callback: () => {
                        this.playFinalAnimAndFinish(clickedHex);
                        this.activeHex.robot.removeLaser();
                        this.endMove();
                    }
                });
            } else if (clickedHex.robot !== null) {
                this.activeHex.robot.drawLaser({x: clickedHex.x, y: clickedHex.y}, {
                    callback: () => {
                        clickedHex.destroyRobot({animate: true});
                        this.activeHex.robot.removeLaser();
                        this.activeHex.robot.animateRobot({x: clickedHex.x, y: clickedHex.y}, {
                            callback: () => {
                                clickedHex.addRobotBasedOn(this.activeHex.robot);
                                if (clickedHex.bonus) {
                                    clickedHex.collectBonus();
                                }
                                const playerNumber = this.activeHex.robot.playerNumber
                                this.activeHex.destroyRobot();
                                this.playerFinishedMove(playerNumber);
                                this.clear();
                                this.endMove();
                            }
                        });
                    }
                });
            } else {
                this.activeHex.robot.animateRobot({x: clickedHex.x, y: clickedHex.y}, {
                    callback: () => {
                        clickedHex.addRobotBasedOn(this.activeHex.robot);
                        if (clickedHex.bonus) {
                            clickedHex.collectBonus();
                        }
                        const playerNumber = this.activeHex.robot.playerNumber
                        this.activeHex.destroyRobot({
                            callback: () => {
                                this.clear();
                                this.endMove();
                            }
                        });
                        this.playerFinishedMove(playerNumber);
                    }
                });
            }
        } else if (clickedHex.state === Hex.states.SHOOTABLE) {
            if (clickedHex.transmitter !== null) {
                this.activeHex.robot.drawLaser({x: clickedHex.x, y: clickedHex.y}, {
                    callback: () => {
                        this.playFinalAnimAndFinish(clickedHex);
                        this.activeHex.robot.removeLaser();
                        this.endMove();
                    }
                });
            } else if (clickedHex.robot !== null) {
                this.activeHex.robot.drawLaser({x: clickedHex.x, y: clickedHex.y}, {
                        callback: () => {
                            clickedHex.destroyRobot({
                                animate: true,
                                callback: () => {
                                    this.activeHex.robot.removeLaser();
                                    this.activeHex.robot.removeShootSkill();
                                    this.playerFinishedMove(this.activeHex.robot.playerNumber);
                                    this.clear();
                                    this.endMove();
                                }
                            });
                        }
                    }
                );
            }
        } else {
            if (clickedHex.state === Hex.states.FREEZEABLE) {
                if (clickedHex.robot !== null) {
                    clickedHex.robot.freezeRobot();
                    this.activeHex.robot.removeFreezeSkill();
                    this.playerFinishedMove(this.activeHex.robot.playerNumber);
                    this.clear();
                }
            } else if (clickedHex.state === Hex.states.OBSTACABLE) {
                clickedHex.markObstructed();
                this.activeHex.robot.removeObstacleSkill();
                this.playerFinishedMove(this.activeHex.robot.playerNumber);
                this.clear();
            }
            this.endMove();
        }
    }

    isTransmitter1AtCoords(y, x) {
        return y === TRANSMITTER_1_POS.x && x === TRANSMITTER_1_POS.y
    }

    isTransmitter2AtCoords(y, x) {
        return y === TRANSMITTER_2_POS.x && x === TRANSMITTER_2_POS.y
    }

    playFinalAnimAndFinish(hexWithTransmitter) {
        const winner = this.getActivePlayer();
        hexWithTransmitter.transmitter.playTakeoverAnimation(() => {
            this.destroyOpponentRobots({
                winner,
                winCallback: () => {
                    this.game.state.start('GameEnd', true, false, {winner});
                }
            })
        })
    }

    destroyOpponentRobots({winner, winCallback}) {
        let c = 0;
        for (let a = 0; a < HEX_H; a++) {
            for (let b = 0; b <= HEX_W; b++) {
                if (this.grid[a][b] && this.grid[a][b].robot && this.grid[a][b].robot.playerNumber !== winner) {
                    c++;
                    game.time.events.add(Phaser.Timer.SECOND / 4 * c, () => {
                        this.grid[a][b].robot.destroyRobot({
                            animate: true,
                            callback: () => {
                                c--;
                                if (c === 0) {
                                    game.time.events.add(Phaser.Timer.SECOND, () => {
                                        winCallback();
                                    });
                                }
                            }
                        })
                    }, this);
                }
            }
        }
    }

    checkAvailableMoves() {
        let player1robots = 0;
        let player1movableRobots = 0;
        let player2robots = 0;
        let player2movableRobots = 0;

        for (let a = 0; a < HEX_H; a++) {
            for (let b = 0; b <= HEX_W; b++) {
                if (this.grid[a][b] && this.grid[a][b].robot) {
                    if (this.grid[a][b].robot.playerNumber === PLAYER_1) {
                        player1robots++;
                    } else if (this.grid[a][b].robot.playerNumber === PLAYER_2) {
                        player2robots++;
                    }
                    if (this.grid[a][b].robot.freezeState === 0) {
                        if (this.grid[a][b].robot.playerNumber === PLAYER_1) {
                            player1movableRobots++;
                        } else if (this.grid[a][b].robot.playerNumber === PLAYER_2) {
                            player2movableRobots++;
                        }
                    }
                }
            }
        }

        if (player1robots === 0) {
            this.game.state.start('GameEnd', true, false, {winner: PLAYER_2});
        } else if (player2robots === 0) {
            this.game.state.start('GameEnd', true, false, {winner: PLAYER_1});
        }
        if (player1movableRobots === 0 && this.getActivePlayer() === PLAYER_1) {
            this.togglePlayerBcNoMove();
        } else if (player2movableRobots === 0 && this.getActivePlayer() === PLAYER_2) {
            this.togglePlayerBcNoMove();
        }
    }
}
