import soundManager from "../instances/SoundManager";
import LocalStorageManager from "../instances/LocalStorageManager";

class MenuMusicManager {

    init() {
        this.soundOn = LocalStorageManager.get('sound_on') !== 'no';
    }

    toggleSound() {
        this.soundOn = !this.soundOn;
        this.startMusic();
    }

    startMusic() {
        if (this.soundOn) {
            soundManager.play('menu', true);
            LocalStorageManager.set('sound_on', 'yes');
        } else {
            soundManager.stop('menu', true);
            LocalStorageManager.set('sound_on', 'no');
        }
    }

    isPlaying() {
        return this.soundOn;
    }
}

const manager = new MenuMusicManager();
manager.init();
export default manager;
